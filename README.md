Directions to commit your existing code:

git config --global user.name "NCI Admin"
git config --global user.email "thencifolks@gmail.com"
cd existing_folder
git init
git remote add origin https://gitlab.com/thencifolks/sample-java-app.git
git add .
git commit -m "Initial commit"
git push -u origin master
